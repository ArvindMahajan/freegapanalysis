package com.rchilli.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.rchilli.api.Base64;
import com.rchilli.entity.FilesInfo;
import com.rchilli.service.ReadJSON;


@Controller
public class GapController {

	String folderLocation = "";
	String apiUrl = "";
	String userKey = "";
	String version = "";
	String subUserId = "";
	String matchType;
	String resumeFile;
	String jdFile;
	String explainScoreType;
	String explainScore;

	@Autowired
	private ServletContext context;
	


	@GetMapping({"/home", "/"})

	public String showHome() {
		return "gap-analysis";

	}

	@PostMapping("/upload")
	public String uploadFiles(RedirectAttributes redirect, @ModelAttribute FilesInfo info) {
		try {
			// get upload folder location from web.xml file context param
			folderLocation = context.getInitParameter("uploadStore"); // get folder location
			// (where you want to store file) as string
			// check wheather that folder is avialble or not , if not create a new folder
			File file = new File(folderLocation); // makes e:/store as File object
			if (!file.exists()) // checks store folder availability in e: dirive
				file.mkdir(); // if not there creates new strore folder
			// get upploaded file names
			String resumeFileName = info.getResume().getOriginalFilename();
			String jdFileName = info.getJd().getOriginalFilename();
			// create InputStreams pointing uploaded files content
			InputStream resumeIs = info.getResume().getInputStream();
			InputStream jdIs = info.getJd().getInputStream();
			// create OutputStreams pointing empty destination files
			OutputStream resumeOs = new FileOutputStream(folderLocation + "/" + resumeFileName);
			OutputStream jdOs = new FileOutputStream(folderLocation + "/" + jdFileName);
			// copy uploaded files content to destination files (complets the file uploading
			// process)
			IOUtils.copy(resumeIs, resumeOs);
			IOUtils.copy(jdIs, jdOs);

			// use of one to one match api

			folderLocation = context.getInitParameter("uploadStore");
			apiUrl = context.getInitParameter("APIURL");
			userKey = context.getInitParameter("UserKey");
			explainScore = context.getInitParameter("explainScore");
			explainScoreType = context.getInitParameter("explainScoreType");
			matchType = info.getMatch();
			
			// File resumeFile = new File(folderLocation);

			String resumeData = Base64.encodeFromFile(folderLocation + "/" + resumeFileName);
			System.out.println(resumeData.substring(0, 10));

			String jdData = Base64.encodeFromFile(folderLocation + "/" + jdFileName);
			
			LinkedHashMap<Object, Object> nest = new LinkedHashMap<>();
			LinkedHashMap<String, Object> link = new LinkedHashMap<>();
			LinkedHashMap<Object, Object> sim = new LinkedHashMap<>();
			boolean b1 = Boolean.valueOf(explainScore);

			// parmeters of api
			sim.put("index", link);
			link.put("indexKey", userKey);
			sim.put("match", nest);
			nest.put("resumeContent", resumeData);
			nest.put("resumeFileName", resumeFileName);
			nest.put("jdContent", jdData);
			nest.put("jdFileName", jdFileName);
			sim.put("matchType", matchType);
			sim.put("explainScore", true);
			sim.put("explainScoreType", "json");

			String val = JSONObject.toJSONString(sim);

			URL url = new URL(apiUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			OutputStream os = conn.getOutputStream();
			os.write(val.getBytes());
			os.flush();
			BufferedReader br = null;
			if (conn.getResponseCode() != 200) {

				br = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
			} else {
				br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			}
			String output;
			StringBuilder sbOutput = new StringBuilder();
			while ((output = br.readLine()) != null) {
				sbOutput.append(output);

			}
			conn.disconnect();
			br.close();
			output = sbOutput.toString();
			System.out.println(output);

			// Get the value from the json objec
			ReadJSON rjs = new ReadJSON();
			LinkedHashMap<String, String> arr = rjs.readJSON(output);
			//String escore = rjs.geteScore();					
			//String eMaxScore= rjs.geteMaxScore();
			String mScore = rjs.getmScore();
			String mMaxScore = rjs.getmMaxScore();
		//	String dScore = rjs.getdScore();
		//	String dMaxScore = rjs.getdMaxScore();
		//	String dEntity = rjs.getdEntity();
		//	String dValue = rjs.getdValue();
		
						
			//put the value in redirect attribut 
			  redirect.addFlashAttribute("mScore", mScore);			  
			  redirect.addFlashAttribute("mMaxScore", mMaxScore);
			 // redirect.addFlashAttribute("dScore", dScore);
			//  redirect.addFlashAttribute("dMaxScore", dMaxScore);
			//  redirect.addFlashAttribute("dEntity", dEntity);
			//  redirect.addFlashAttribute("dValue", dValue);
			  
			  ArrayList<String>ss=new ArrayList<>();
			  
			  for(int i=0;i<rjs.getDd().size();i++) {
				  
				  ss.add(rjs.getDe().get(i));
				  ss.add(rjs.getDd().get(i));
				  ss.add(rjs.getDs().get(i));
				  ss.add(rjs.getDm().get(i));
				  
		 }
			  
			  
			  redirect.addFlashAttribute("list", ss);	
			  redirect.addFlashAttribute("showError", true);
			 
			
			// close streams
			resumeOs.close();
			resumeIs.close();
			jdOs.close();
			jdIs.close();
			// return lvn

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return "redirect:home";

	}

}
