package com.rchilli;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FreeGapAnalysisApplication {

	public static void main(String[] args) {
		SpringApplication.run(FreeGapAnalysisApplication.class, args);
	}

}
