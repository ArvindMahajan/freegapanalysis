package com.rchilli.service;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.google.gson.JsonObject;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpSession;

public class ReadJSON {

	String eScore = "";
	String eMaxScore = "";
	String mScore = "";
	String mMaxScore = "";
	String dScore = "";
	String dMaxScore = "";
	String dEntity = "";
	String dValue = "";
	
     ArrayList<String > ds=new ArrayList<>();
     ArrayList<String > dm=new ArrayList<>();
     ArrayList<String > de=new ArrayList<>();
     ArrayList<String > dd=new ArrayList<>();

   

	public String geteScore() {
		return eScore;
	}

	public String geteMaxScore() {
		return eMaxScore;
	}

	public String getmScore() {
		return mScore;
	}

	public String getmMaxScore() {
		return mMaxScore;
	}

	public String getdScore() {
		return dScore;
	}

	public String getdMaxScore() {
		return dMaxScore;
	}

	public String getdEntity() {
		return dEntity;
	}

	public String getdValue() {
		return dValue;
	}
	
	public ArrayList<String> getDs() {
		return ds;
	}

	public ArrayList<String> getDm() {
		return dm;
	}

	public ArrayList<String> getDe() {
		return de;
	}

	public ArrayList<String> getDd() {
		return dd;
	}

	public LinkedHashMap<String, String> getData() {
		return Data;
	}

	LinkedHashMap<String, String> Data = new LinkedHashMap<String, String>();

	public LinkedHashMap<String, String> readJSON(String rchilliResponse) {

		try {

			if (!rchilliResponse.contains("\"errormsg\":")) {

				JSONParser parser = new JSONParser();

				Object outputObject = parser.parse(rchilliResponse.toString());
				JSONObject root = (JSONObject) outputObject;
				JSONArray ja_data = (JSONArray) root.get("explainScore");

				for (int i = 0; i < ja_data.size(); i++) {
					// Explanation object
					JSONObject eObj = (JSONObject) ja_data.get(i);
					JSONObject explantaion = (JSONObject) eObj.get("explaination");
					eScore = explantaion.get("score").toString();
					eMaxScore = explantaion.get("maxScore").toString();
					JSONObject eMatch = (JSONObject) explantaion.get("Match");

					// match object
					
					mScore = eMatch.get("score").toString();
					mMaxScore = eMatch.get("maxScore").toString();
					JSONArray mDetailScore = (JSONArray) eMatch.get("detailScore");

					for (int j = 0; j < mDetailScore.size(); j++) {
						JSONObject dOb = (JSONObject) mDetailScore.get(j);												
			
						dm.add(dOb.get("maxScore").toString());
						ds.add(dOb.get("score").toString());
						de.add( dOb.get("entity").toString());
						dd.add( dOb.get("value").toString());
						
						 
					}
					
				}

				return Data;
			}
		} catch (Exception e) {

		}
		return Data;
	}


}
