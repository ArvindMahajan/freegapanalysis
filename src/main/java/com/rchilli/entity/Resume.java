package com.rchilli.entity;

import java.io.Serializable;

public class Resume implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String indexKey;
	private String resuemKey;
	private String resumeFileName;
	private String jdContent;
	private String jdFileName;
	private String matchType;
	
	
	public Resume() {
	
	}


	public Resume(String indexKey, String resuemKey, String resumeFileName, String jdContent, String jdFileName,
			String matchType) {
		super();
		this.indexKey = indexKey;
		this.resuemKey = resuemKey;
		this.resumeFileName = resumeFileName;
		this.jdContent = jdContent;
		this.jdFileName = jdFileName;
		this.matchType = matchType;
	}


	public String getIndexKey() {
		return indexKey;
	}


	public void setIndexKey(String indexKey) {
		this.indexKey = indexKey;
	}


	public String getResuemKey() {
		return resuemKey;
	}


	public void setResuemKey(String resuemKey) {
		this.resuemKey = resuemKey;
	}


	public String getResumeFileName() {
		return resumeFileName;
	}


	public void setResumeFileName(String resumeFileName) {
		this.resumeFileName = resumeFileName;
	}


	public String getJdContent() {
		return jdContent;
	}


	public void setJdContent(String jdContent) {
		this.jdContent = jdContent;
	}


	public String getJdFileName() {
		return jdFileName;
	}


	public void setJdFileName(String jdFileName) {
		this.jdFileName = jdFileName;
	}


	public String getMatchType() {
		return matchType;
	}


	public void setMatchType(String matchType) {
		this.matchType = matchType;
	}
	
	
}
