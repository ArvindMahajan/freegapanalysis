package com.rchilli.entity;

import org.springframework.web.multipart.MultipartFile;

public class FilesInfo {
	private String match;
	private MultipartFile resume;
	private MultipartFile jd;

	public String getMatch() {
		return match;
	}

	public void setMatch(String match) {
		this.match = match;
	}

	public MultipartFile getResume() {
		return resume;
	}

	public void setResume(MultipartFile resume) {
		this.resume = resume;
	}

	public MultipartFile getJd() {
		return jd;
	}

	public void setJd(MultipartFile jd) {
		this.jd = jd;
	}

}
