<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>ParsedResult</title>
<link rel="stylesheet" href="css/bootstrap.min.css" />
<!-- <link rel="stylesheet" href="Roboto-Regular/styles.css"> -->
<link rel="stylesheet" href="css/animate.min.css" />
<link rel="stylesheet" href="scss/main.css " />
<link rel="stylesheet" href="scss/responsive.css" />

<link
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
	rel="stylesheet" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/balloon-css/0.5.0/balloon.min.css">
</head>



<style type="text/css">
.modal-backdrop {
	position: fixed;
	top: 0;
	left: 0;
	z-index: 0 !important;
	width: 100vw;
	height: 100vh;
	background-color: #000;
}
/*.bg-img{
 height: 100%;
 background-repeat: no-repeat;
 background-position: center;
 background-size: cover;
 background-attachment: fixed;
}*/
.match_email_score {
	color: #15856a;
	font-weight: bold;
}

#one_to_one_match .fa {
	color: #15856a;
}

.match_sbmt_btn {
	background-color: #158569;
	width: 100%;
	height: 40px;
	border-color: #158569;
	font-size: 16px;
	border-radius: 4px;
	color: #ffffff;
	padding-left: 0;
	font-weight: bold;
}

.match_sbmt_btn:focus {
	background-color: #158569;
	width: 100%;
	height: 40px;
	border-color: #158569;
	font-size: 16px;
	border-radius: 4px;
	color: #ffffff;
	padding-left: 0;
	font-weight: bold;
}

.match_sbmt_btn:hover {
	background-color: #158569;
	width: 100%;
	height: 40px;
	border-color: #158569;
	font-size: 16px;
	border-radius: 4px;
	color: #ffffff;
	padding-left: 0;
	font-weight: bold;
}

.gap-analysis-bg-img {
	display: inline-block;
	background-image: url(./img/gap-analysis.png);
	background-size: cover;
	background-color: transparent;
	background-repeat: no-repeat;
	width: 100%;
	height: 70vh;
}

.first-intro {
	color: #ffffff;
}

.page-intro-content {
	color: #ffffff;
}

.h5, h5 {
	font-size: 2.20rem;
}

#one_to_one_match .send-mail-btn {
	background-color: #158569;
	border: 2px solid #158569;
	color: #ffffff;
}
</style>
<body>
	<!-- sidebar-wrapper  -->


	<div class="container-fluid gap-analysis-bg-img px-0"
		id="one_to_one_match">
		<div class="row">
			<div class="col-md-12">
				<div class="container">
					<div class="row">
						<div class="col-md-12 first-intro pt-2">
							<h6>
								<!--  <img  src="img/rchilllogo.png"/> -->
								Rank Your Resume

							</h6>
						</div>
						<div class="col-md-12 text-center page-intro-content  ">
							<h5>Get FREE Evaluation of Your Resume</h5>
							<p>
								Our AI-powered resume ranker analyzes your resume and offers
								actionable steps you can<br> take to revamp your resume and
								get your dream job.
							</p>
						</div>
					</div>
					<div class="row m-4 py-4 shadow" style="background-color: #f9f9f9;">
						<div class="col-md-12">
							<div class="row px-4 mx-2 parsing-box rounded "
								style="background-color: #ffffff;">
								<div class="col-md-12 p-3">
									<form action="upload" method="post"
										enctype="multipart/form-data" id="formsubmit">
										<div class="row pt-3">
											<div class="col-md-12 px-0 py-2">
												<h3>GAP ANALYSIS</h3>
											</div>

											<div class="col-md-6 pl-0 search_input">
												<label>Select Resume</label> <span
													style="color: #bebcbc; font-size: 12px;"
													data-balloon-length="large"
													data-balloon=" FreeGap support only doc, docx, dot, rtf, pdf, odt, txt, htm and html files."
													data-balloon-pos="up"><i class="fa fa-info-circle"
													aria-hidden="true"></i></span>
												<div class="custom-file overflow-hidden">
													<input id="resumeFile" type="file"
														class="custom-file-input" name="resume"
														style="cursor: pointer;" required="required"
														onchange="ValidateSingleInput(this);"> <label
														for="customFile" id="custom" class="custom-file-label">Upload
														Resume </label>
													<div class="row" style="display: none;" id="resumeError">
														<div class="col-md-12">
															<p class="file_upld_msg ">Please select a valid file.
																Gap Analysis support only doc, docx, dot, rtf, pdf, odt,
																txt, htm and html files.</p>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-6 pr-0 search_input">
												<label>Select JD</label> <span
													style="color: #bebcbc; font-size: 12px;"
													data-balloon-length="large"
													data-balloon=" FreeGap support only doc, docx, dot, rtf, pdf, odt, txt, htm and html files."
													data-balloon-pos="up"><i class="fa fa-info-circle"
													aria-hidden="true"></i></span>
												<div class="custom-file overflow-hidden">
													<input id="jdFile" type="file" class="custom-file-input"
														name="jd" style="cursor: pointer;" required="required"
														onchange="ValidateSingleInput(this);"> <label
														for="customFile" id="jdcustom" class="custom-file-label">Upload
														JD </label>

													<div class="col-md-12">
														<p class="file_upld_msg ">Please select a valid file.
															Gap Analysis support only doc, docx, dot, rtf, pdf, odt,
															txt, htm and html files.</p>
													</div>
												</div>
											</div>


										</div>
										<div class="row pt-3">
											<div class="col-md-6 pl-0 search_input">

												<div class="form-group">
													<label for="feedback">Select Matching Type</label> <select
														class="form-control" style="cursor: pointer;" name="match">
														<option selected="">Resume to JD</option>
														<option>JD to Resume</option>
													</select>
												</div>

											</div>
											<div class="col-md-6 pr-0 search_input">
												<div class="submit-btn-plc">
													<button type="button" id="buttonMatch"
														class="btn btn-block font-weight-bold px-4 match_sbmt_btn">Submit</button>
												</div>
											</div>

										</div>
									</form>


								</div>


								<img id="loader" src="./img/loader.gif"
									style="position: absolute; top: 50%; left: 50%; margin-top: -50px; margin-left: -50px; width: 100px; height: 100px;" />

								<%-- <div class="modal" id="loadingModal" style="display: none;" aria-hidden="true">
									<div  class="modal-dialog modal-dialog-centered loading-modal animated"
										style="animation-name: zoomIn" role="document">
										<div class="modal-content">
											<div class="modal-body text-center">
                                              <img id = "loader" src="<c:url value="/img/loader.gif" />" style = "display:none; position: absolute; top: 50%; left: 50%; margin-top: -50px; margin-left: -50px; width: 100px; height: 100px;"/>
												<div class="progress">
													<div class="progress-bar" role="progressbar"
														style="width: 60%" aria-valuenow="25" aria-valuemin="0"
														aria-valuemax="100"></div>
												</div>
												<p class="loading-dots">Loading</p>
											</div>
										</div>
									</div>
								</div> --%>
								<div class="col-md-6 ml-0 pt-3">
									<div class="row mr-0">

										<div class="col-md-5 mb-2 match-score-box-a border rounded">


											<span class="font-weight-bold">Matched Score : </span>
											${mScore}



										</div>

										<div class="col-md-2"></div>
										<div class="col-md-5 mb-2 match-score-box-a border rounded">

											<span class="font-weight-bold">Max Matched Score : </span>${mMaxScore}


										</div>

									</div>

								</div>
								<br> <br>
								<c:choose>
									<c:when test="${mMaxScore ne null && !empty mScore }">
									</c:when>
									<c:otherwise>

										<c:if test="${showError eq true }">
											<div id="errorDialogue" class="modal show" aria-modal="true"
												style="padding-right: 17px; display: block;">
												<div
													class="modal-dialog succ-fail-modal error-modal modal-dialog-centered ">
													<div class="modal-content special-modal">
														<div class="modal-circle-top"></div>
														<div class="modal-header border-0">

															<button type="button" class="close" data-dismiss="modal"
																data-balloon-length="small" data-balloon="Close"
																data-balloon-pos="up" onclick="hide_modal();">
																<span aria-hidden="true">�</span>
															</button>
														</div>
														<div class="modal-body text-center">
															<h3 id="errorDialogue">Matching Unsuccessful</h3>
															<p id="errorDialogue">This Resume is not matching
																with JD.</p>
															<button class="btn btn-lg btn-block " type="button"
																data-dismiss="modal" onclick="hide_modal();">
																Close</button>
														</div>
													</div>
												</div>
											</div>

										</c:if>
									</c:otherwise>
								</c:choose>

								<div class="col-md-6 pt-3"></div>
								<div class="col-md-12 px-0 pt-4">
									<div class="row pt-4">
										<div class="col-md-6 getting-started">
											<h4>
												<span class="d-block">Your Detailed Matched Result</span>
											</h4>
										</div>
										<div class="col-md-6 match_email_score" align="right">
											<span data-toggle="modal" data-target="#exampleModal"
												style="cursor: pointer;"><i class="fa fa-envelope"
												aria-hidden="true"></i> Email Score Details</span>
											<div class="modal fade" id="exampleModal" tabindex="-1"
												role="dialog" aria-labelledby="exampleModalLabel"
												aria-hidden="true">
												<div class="modal-dialog" role="document">
													<div class="modal-content">
														<div class="modal-header">
															<h4 class="modal-title email-pop-up-head"
																id="exampleModalLongTitle">Share Email Score
																Details to</h4>
															<button type="button" class="close" data-dismiss="modal"
																aria-label="Close" data-balloon-length="small"
																data-balloon="Close" data-balloon-pos="up">
																<span aria-hidden="true">�</span>
															</button>
														</div>
														<div class="modal-body pr-0">
															<div class="row pb-3">
																<div class="col-md-2 text-left px-0">
																	<b>Email Id(s):</b>
																</div>
																<div class="col-md-10 px-0">
																	<input type="text"
																		class="link-bar w-100  validate[required]"
																		id="devMail" name="devMail"
																		placeholder="Enter multiple emails with comma seperator">
																</div>
															</div>
														</div>
														<div class="modal-footer">
															<!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
															<button type="button" class="btn send-mail-btn">Send</button>
														</div>
													</div>
												</div>
											</div>

										</div>
									</div>
									<div class="row ">
										<div class="col-md-12 px-0">
											<section class="panel">
												<div class="panel-body">
													<div id="example_wrapper"
														class="dataTables_wrapper dt-bootstrap4 no-footer">
														<div class="row">
															<div class="col-sm-12">
																<table id="example"
																	class="table table-striped table-bordered dataTable no-footer"
																	style="width: 100%" role="grid"
																	aria-describedby="example_info">
																	<thead>
																		<tr role="row">
																			<th class="sorting" tabindex="0"
																				aria-controls="example" rowspan="1" colspan="1"
																				aria-label="Source: activate to sort column ascending"><span
																				data-balloon-length="large"
																				data-balloon="Scores are based on Matching Fields(e.g. Job Profile, Skills, Education, Level of Experience Required, City, State, Country, etc.)"
																				data-balloon-pos="up">Matching Field</span></th>
																			<th class="sorting" tabindex="0"
																				aria-controls="example" rowspan="1" colspan="1"
																				aria-label="Status: activate to sort column ascending"><span
																				data-balloon-length="large"
																				data-balloon="Value of entity that matches between the documents"
																				data-balloon-pos="up">Matched Value</span></th>
																			<th class="sorting_asc" oaria="sSortDescending"
																				tabindex="0" aria-controls="example" rowspan="1"
																				colspan="1" aria-sort="ascending"
																				aria-label="Parse Date: activate to sort column descending"><span
																				data-balloon-length="large"
																				data-balloon="Matching Field Score"
																				data-balloon-pos="up">Score<span></span></span></th>
																			<th class="so223.6pxrting" tabindex="0"
																				aria-controls="example" rowspan="1" colspan="1"
																				aria-label="File Name: activate to sort column ascending"><span
																				data-balloon-length="large"
																				data-balloon="Segregation of the Total Score"
																				data-balloon-pos="up">Max score</span></th>
																		</tr>
																		<!-- 	<tr role="row">
																				<c:forEach var="e" items="${Entity}">
																					<td>${e}</td>
																					<td></td>
																				
																				</c:forEach>

																			</tr> -->
																	</thead>
																	<tbody>
																		<%
																		int i = 1;
																		%>
																		<tr role="row" class="odd">
																			<c:forEach var="e" items="${list}">

																				<td>${e}</td>
																				<%
																				if (i == 4) {
																				%>
																				<tr></tr>
																				<%
																				i = 1;
																				} else {
																				i++;
																				}
																				%>
																			</c:forEach>
																		</tr>
																	</tbody>

																</table>
															</div>
														</div>
													</div>
												</div>
											</section>
										</div>
									</div>

								</div>
							</div>
						</div>

					</div>

				</div>
			</div>

		</div>
		<footer style="height: 50px; background-color: #145198;">
			<div class="text-center p-3"
				style="background-color: #145198; color: #fff; height: 30px;">
				<p>
					Copyright &copy;
					<script>
						document.write(new Date().getFullYear())
					</script>
					RChilli. All Rights Reserved
				</p>

			</div>
		</footer>
	</div>

	<!-- page-content -->

	<!-- page-wrapper -->
	<!-- Footer -->
	<script src="js/jquery.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>


	<script src="js/jquery-ui.min.js"></script>
	<script src="js/customjs.js"></script>
	<script src="js/jquery.fileupload.js"></script>

	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script>
		$(document).ready(function() {
			$("#resumeFile").change(function() {
				if ($("#resumeFile")[0].files.length == 0) {
					$('#custom').html('Upload Resume');
				} else {
					var file = this.files[0].name;
					$('#custom').html(file);
				}
			});

			$("#jdFile").change(function() {
				if ($("#jdFile")[0].files.length == 0) {
					$('#jdcustom').html('Upload Resume');
				} else {
					var file = this.files[0].name;
					$('#jdcustom').html(file);
				}
			});
		});

		function hide_modal() {
			$('#errorDialogue').hide();
		}

		var _validFileExtensions = [ ".txt", ".docx", ".docs", ".pdf", ".html",
				".dot", ".dotx", ".docm", ".dotm", ".odt", ".rtf", ".htm",
				".html", ".doc" ];
		function ValidateSingleInput(oInput) {
			if (oInput.type == "file") {
				var sFileName = oInput.value;
				if (sFileName.length > 0) {
					var blnValid = false;
					for (var j = 0; j < _validFileExtensions.length; j++) {
						var sCurExtension = _validFileExtensions[j];
						if (sFileName.substr(
								sFileName.length - sCurExtension.length,
								sCurExtension.length).toLowerCase() == sCurExtension
								.toLowerCase()) {
							blnValid = true;
							break;
						}
					}

					if (!blnValid) {
						alert("FreeGap Analysis supports only "
								+ _validFileExtensions.join(",")
								+ " extenstion");
						oInput.value = "";
						return false;
					}
				}
			}
			return true;
		}
	</script>

	<script>
		$(document).ready(function() {
			$('#loader').hide();
			$("#buttonMatch").click(function() {

				$('#loader').show();
				$('#formsubmit').submit();
			});

		});
	</script>


	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

</body>



</html>