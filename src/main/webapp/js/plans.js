var planId = '';
function closeSuccess() {
	$('#success-container').hide(200);
	// window.location.replace('plans');
}

function openPortal() {
	$.ajax({
		type : "POST",
		url : "getChargebeePortalLink",
		cache : false,
		success : function(html) {
			html = html.trim();
			hideLoader();
			if (html == 'refresh') {
				window.location.reload();
			} else if (html == 'notExist') {
				setErrorText('There is no active subscription.');
				showError();
			} else if (html == '') {
				setErrorText('Could not open portal.');
				showError();
				reloadPage(7000);
			} else {
				window.open(html, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,left=500,width=800,height=800");
			}
		}
	});
};

function startUpPlan(planId1) {
	planId = planId1;
	newSubscription();
}

function newSubscription() {
	showLoader();
	$.ajax({
		type : "POST",
		data : {
			'planId' : planId
		},
		url : "redirectToEverSign",
		success : function(data) {
			data = data.trim();
			var obj = JSON.parse(data);
			if (obj.refresh) {
				window.location.reload();
			} else if (!obj.success) {
				if (obj.message == 'existing') {
					updateSubscription(false);
					return;
				} else {
					hideLoader();
					setErrorText(obj.message);
					showError();
					reloadPage(7000);
					return;
				}
			} else {
				window.location.replace(obj.message);
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			setErrorText("There is an error.");
			showError();
			reloadPage(7000);
		}
	});
}

function postWaring() {
	if (newSubscriptionVar) {
		newSubscription();
	} else {
		updateSubscription(true);
	}
}

function updateSubscription(terms) {
	closeWarning();
	showLoader();
	$.ajax({
		type : "POST",
		data : {
			'planId' : planId,
			'terms' : terms
		},
		url : "UpdateSubscription",
		success : function(data) {
			data = data.trim();
			if (data === "refresh") {
				window.location.reload();
				return;
			} else if (data === "success") {
				hideLoader();
				setSuccessText("Subscription has been updated");
				showSuccess();
				reloadPage(7000);
				return;
			} else if (data === "newSubscribe") {
				newSubscription();
				return;
			} else {
				hideLoader();
				setErrorText(data);
				showError();
				reloadPage(7000);
				return;
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			setErrorText("There is an error.");
			showError();
			reloadPage(7000);
		}
	});
}

$(".addToMyPlan").on("click", function() {
	var id = $(this).attr('id');
	var add = '';
	if ($(this).text() === 'Add To My Plan') {
		add = 'true';
	} else {
		add = 'false';
	}
	$.ajax({
		type : "POST",
		data : {
			'addonId' : id,
			'action' : 'addon',
			'add' : add
		},
		url : "addonHelper",
		cache : false,
		success : function(data) {
			data = data.trim();
			if (data === "added") {
				$('.addToMyPlan').text('Remove');
			} else if (data === "removed") {
				$('.addToMyPlan').text('Add To My Plan');
			}
		}
	});
});

$(".leftArrow").click(function() {
	var hideLArrow = false;
	var showLArrow = false;
	$(".tabcontent").each(function() {
		if ($(this).css('display') == 'block') {
			$(this).find(".pricing").animate({
				scrollLeft : "-=400px"
			}, 800);
			var currentPosition = $(this).find('.pricing').scrollLeft() - 400;
			if (currentPosition < 1) {
				hideLArrow = true;
			} else {
				showLArrow = true;
			}
			return false;
		}
	});
	if (hideLArrow) {
		$(this).hide();
	}
	if (showLArrow) {
		$(this).show();
	}
	$(this).siblings(".rightArrow").show();
});
$(".rightArrow").click(function() {
	var hideRArrow = false;
	var showRArrow = false;
	$(".tabcontent").each(function() {
		if ($(this).css('display') == 'block') {
			$(this).find(".pricing").animate({
				scrollLeft : "+=400px"
			}, 800);
			var totalWidth = $(this).find('.pricing').get(0).scrollWidth - $(this).find('.pricing').width();
			var currentPosition = $(this).find('.pricing').scrollLeft() + 400;
			if (currentPosition > totalWidth) {
				currentPosition = totalWidth;
			}
			if ((totalWidth - currentPosition) < 1) {
				hideRArrow = true;
			} else {
				showRArrow = true;
			}
			return false;
		}
	});

	$(this).siblings(".leftArrow").show();
	if (hideRArrow) {
		$(this).hide();
	}
	if (showRArrow) {
		$(this).show();
	}
});

var totalAmount = 0;
function updateTotalAmount(newAmount) {
	totalAmount = parseFloat(totalAmount) + parseFloat(newAmount);
}

var newSubscriptionVar = false;
$(".managePlan").click(
		function() {
			var text = $(this).html();
			var $plan = $(this).closest('.card-pricing');
			var $currentPlan = $('.selected');
			planId = $plan.find(".planId").html();
			if (text == 'Manage') {
				showLoader();
				openPortal();
				return;
			} else if (text == 'Subscribe') {
				newSubscriptionVar = true;
			}
			var price = $plan.find(".planPrice").html();
			var price2 = price.substr(price.indexOf('$') + 1, price.length).trim();
			if (price2.includes(',')) {
				price2 = price2.replace(',', '').trim();
			}
			totalAmount = 0;
			updateTotalAmount(price2);

			if (!newSubscriptionVar) {
				var currentPlanPrice = $currentPlan.find(".planPrice").html();
				var currentPlanName = $currentPlan.find(".planName").html();
				setCurrentPlan(currentPlanName + " " + formatPrice(currentPlanPrice));
			}
			var name = $plan.find(".planName").html();
			var period = $plan.find(".planPeriod").html();
			setDateOfChange();
			setNewPlan('<div class="col-md-7 p-0"><h6 class="m-0">' + name
					+ '</h6></div><div class="col-md-5 p-0"><h6 class="m-0 float-right">' + formatPrice(price) + '</h6></div>');

			$('#checkboxTerms').prop('checked', true);
			getApplicableAddons(planId);

		});

function setAddonsInCurrentPlan() {
	$('.addon-switches').each(function() {
		if ($(this).is(":checked")) {
			$("#currentPlan").append('<br>' + $(this).attr('name'));
		}
	});
}

function postAddon() {
	closeAddonsContainer();
	showLoader();
	var addonsEnabled = [];
	var addTotal = false;
	$('.addon-switches').each(
			function() {
				if ($(this).is(":checked")) {
					if ($(this).attr('price') == '0') {
						$("#newPlan").append('<div class="col-md-7 p-0"><h6 class="m-0">' + $(this).attr('name') + '</h6></div>');
					} else {
						$("#newPlan").append(
								'<div class="col-md-7 p-0"><h6 class="m-0">' + $(this).attr('name')
										+ '</h6></div><div class="col-md-5 p-0"><h6 class="m-0 float-right">$ '
										+ parseFloat($(this).attr('price')).toLocaleString() + '</h6></div>');
						updateTotalAmount($(this).attr('price'));
						addTotal = true;
					}

					addonsEnabled.push($(this).attr('id'));
				}
			});
	if (addTotal) {
		$("#newPlan")
				.append(
						'<div class="col-md-7 p-0"><h6 class="m-0"> Total </h6></div><div class="col-md-5 p-0"><hr class="m-0 p-0 w-75 float-right"><h6 class="m-0 float-right">$ '
								+ parseFloat(totalAmount).toLocaleString() + '</h6></div>');
	}
	if (addonsEnabled.length == 0) {
		addonsEnabled[0] = 'dummy';
	}
	$.ajax({
		type : "POST",
		url : "setAddonChanges",
		data : {
			'addons' : addonsEnabled
		},
		success : function(data) {
			var obj = JSON.parse(data);
			if (obj.refresh) {
				window.location.reload();
				return;
			} else if (obj.success) {
				hideLoader();
				$(".sub-btn").attr("disabled", false);
				if (newSubscriptionVar) {
					$('#currentPlanDiv').hide();
					$('#agreementDiv').hide();
					$('#dateText').html('Date of Purchase:');
					showWarning();
				} else {
					$('#currentPlanDiv').show();
					$('#agreementDiv').show();
					$('#dateText').html('Date of Change:');
					showWarning();
				}
			} else {
				setErrorText(obj.message);
				showError();
				return;
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			setErrorText("There is an error.");
			showError();
		}
	});
}

function addonChange() {
	e.preventDefault();
	var isChecked = $('#addon').is(":checked");
	showLoader();
	$.ajax({
		type : "POST",
		url : "addAddonToSubscription",
		success : function(data) {
			data = data.trim();
			hideLoader();
			if (data === "refresh") {
				window.location.reload();
				return;
			} else if (data === "success") {
				if (isChecked) {
					$('#addon').prop('checked', false);
					setSuccessText("Addon has been disabled");
				} else {
					setSuccessText("Addon has been enabled");
					$('#addon').prop('checked', true);
				}
				showSuccess();
				reloadPage(7000);
				return;
			} else {
				setErrorText(data);
				showError();
				reloadPage(7000);
				return;
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			setErrorText("There is an error.");
			showError();
		}
	});
}
function ocrAddonChange() {
	var isChecked = $('#ocr-addon').is(":checked");
	showLoader();
	$.ajax({
		type : "POST",
		url : "addOCRAddonToSubscription",
		success : function(data) {
			data = data.trim();
			hideLoader();
			if (data === "refresh") {
				window.location.reload();
				return;
			} else if (data === "success") {
				if (isChecked) {
					$('#ocr-addon').prop('checked', false);
					setSuccessText("OCR Addon has been disabled");
				} else {
					setSuccessText("OCR Addon has been enabled");
					$('#ocr-addon').prop('checked', true);
				}
				showSuccess();
				reloadPage(7000);
				return;
			} else {
				if (data.includes('There is an error')) {
					setErrorText(data);
					showError();
				} else {
					setAlertText(data);
					showAlert();
				}
				reloadPage(7000);
				return;
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			setErrorText("There is an error.");
			showError();
			reloadPage(7000);
		}
	});
}
function taxonomyAddonChange() {
	var isChecked = $('#taxonomy-addon').is(":checked");
	showLoader();
	$.ajax({
		type : "POST",
		url : "addTaxonomyAddonToSubscription",
		success : function(data) {
			data = data.trim();
			hideLoader();
			if (data === "refresh") {
				window.location.reload();
				return;
			} else if (data === "success") {
				if (isChecked) {
					$('#taxonomy-addon').prop('checked', false);
					setSuccessText("Taxonomy Addon has been disabled");
				} else {
					setSuccessText("Taxonomy Addon has been enabled");
					$('#taxonomy-addon').prop('checked', true);
				}
				showSuccess();
				reloadPage(7000);
				return;
			} else if (data === "scheduled") {
				setSuccessText("Taxonomy Addon change has been scheduled");
				showSuccess();
				reloadPage(7000);
				return;
			} else if (data === "removed") {
				setSuccessText("Taxonomy Addon scheduled change has been removed");
				showSuccess();
				reloadPage(7000);
				return;
			} else {
				if (data.includes('There is an error')) {
					setErrorText(data);
					showError();
				} else {
					setAlertText(data);
					showAlert();
				}
				reloadPage(7000);
				return;
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			setErrorText("There is an error.");
			showError();
			reloadPage(7000);
		}
	});
}
var addonHTMLId;
$('.addon-alert').on("click", function(e) {
	e.preventDefault();
	var isChecked = !$(this).is(":checked");
	addonHTMLId = $(this).attr('id');
	var message = 'Are you sure you want to enable Add-on? <br><br> Note : This will override your scheduled addon changes (if any).';
	if (isChecked) {
		message = 'Are you sure you want to disable Add-on? <br><br> Note : This will override your scheduled addon changes (if any).';
	}
	$('#addon-change-message').html(message);
	$('#addon-alert-container').show(200);
});
function closeAddonsAlert() {
	hideLoader();
	$('#addon-alert-container').hide(200);

}
function reloadPage(x) {
	setTimeout(function() {
		window.location = 'plans';;
	}, x);
}
function procceedAddonChange() {
	closeAddonsAlert();
	if (addonHTMLId == 'se-addon') {
		seAddonChange()
	} else if (addonHTMLId == 'taxonomy-addon') {
		taxonomyAddonChange();
	} else if (addonHTMLId == 'ocr-addon') {
		ocrAddonChange();
	} else if (addonHTMLId == 'addon') {
		addonChange();
	}
}

function seAddonChange() {

	var isChecked = $('#se-addon').is(":checked");
	showLoader();
	$.ajax({
		type : "POST",
		url : "addSEAddonToSubscription",
		success : function(data) {
			data = data.trim();
			hideLoader();
			if (data === "refresh") {
				window.location.reload();
				return;
			} else if (data === "success") {
				if (isChecked) {
					$('#se-addon').prop('checked', false);
					setSuccessText("Search Engine Addon has been disabled");
				} else {
					setSuccessText("Search Engine Addon has been enabled");
					$('#se-addon').prop('checked', true);
				}
				showSuccess();
				reloadPage(7000);
				return;
			} else if (data === "scheduled") {
				setSuccessText("Search Engine Addon change has been scheduled");
				showSuccess();
				reloadPage(7000);
				return;
			} else {
				if (data.includes('There is an error')) {
					setErrorText(data);
					showError();
				} else {
					setAlertText(data);
					showAlert();
				}
				reloadPage(7000);
				return;
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			setErrorText("There is an error.");
			showError();
			reloadPage(7000);
		}
	});
}

var addonpopHTML;

function setAddonpopHTML() {
	addonpopHTML = $('#addons_data_container').html();
}

function processAddonData(data) {
	try {
		data = data.trim();
		var obj = JSON.parse(data);
		if (obj.refresh) {
			window.location.reload();
			return;
		} else if (obj.success) {
			removeAddonsInpopup();
			$.each(obj.message, function(index, value) {
				setAddonsInpopup(value);
			});
			hideLoader();
			setAddonsInCurrentPlan();
			showAddonsContainer();
		}
	} catch (e) {
		console.log(e);
	}

}
function removeAddonsInpopup() {
	$('#addons_data_container').html('');
}

function setAddonsInpopup(data) {
	var name;
	var code;
	var price;
	var priceCustom = '';
	var active = false;
	$.each(data, function(key, value) {
		if (key == 'name') {
			name = value;
		} else if (key == 'code') {
			code = value;
		} else if (key == 'active' && value == 'true') {
			active = true;

		} else if (key == 'price') {
			price = value;

		} else if (key == 'priceCustom') {
			priceCustom = value;
		}
	});
	if (addonpopHTML != null && name != null && code != null && price != null) {
		var newHTML = addonpopHTML.replaceAll('{name}', name).replace('{code}', code).replace('{price}', price);
		if (active) {
			newHTML = newHTML.replace('{checked}', 'checked');
		} else {
			newHTML = newHTML.replace('{checked}', '');
		}
		newHTML = newHTML.replace('{priceCustom}', priceCustom);
		$("#addons_data_container").append(newHTML);
	}
}
function getApplicableAddons(planId) {
	showLoader();
	$.ajax({
		type : "POST",
		url : "getApplicableAddons",
		data : {
			'planId' : planId
		},
		success : function(data) {
			processAddonData(data);
			return;
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			setErrorText("There is an error.");
			showError();
		}
	});
}

function goToYearly(planName, currentElement) {
	var scrollLeftDiv = $(currentElement).parents('.pricing').scrollLeft();
	$(".data-box").each(function() {
		if ($(this).css('display') == 'block') {
			var id = $(this).prop('id');
			var divLeft = $(this).find(".pricing").scrollLeft;
			$(this).find(".pricing").animate({
				scrollLeft : "-=1000px"
			}, 10);
			openTab(null, id + 'Yearly');
			$(this).find('.pricing').animate({
				scrollLeft : scrollLeftDiv
			}, 1000);
		}
	});
}
function formatPrice(price) {
	price = price.replace('Billed', '').trim();
	if (price.includes('Billing')) {
		price = price.replace('Billing', '').trim();
	}
	var period = price.substr(0, price.indexOf('as')).trim();
	price = price.substr(price.indexOf('$'), price.length).trim();
	return period + " " + price;
}
function closeWarning() {
	hideLoader();
	$('#warning-container').hide(200);
}
function showWarning() {
	$(".sub-btn").attr("disabled", false);
	$("#checkboxTerms").attr("checked", true);
	$('#warning-container').show(200);
}
function closeAddonsContainer() {
	$('#addons-container').hide(200);
}
function showAddonsContainer() {
	$('#addons-container').show(200);
}
function setCurrentPlan(x) {
	$('#currentPlan').html(x);
}
function setNewPlan(x) {
	$('#newPlan').html(x);
}
function setDateOfChange() {
	$('#dateOfChange').html($.datepicker.formatDate('mm-dd-yy', new Date()));
}

$('#checkboxTerms').change(function() {
	if (this.checked) {
		$(".sub-btn").attr("disabled", false);
	} else {
		$(".sub-btn").attr("disabled", true);
	}
});
