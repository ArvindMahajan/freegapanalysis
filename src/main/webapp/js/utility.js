document.onreadystatechange = function() {
	if (document.readyState !== "complete") {
		var css = $(".page-content").css('display');
		var css2 = $("body").css('display');
		if (css == 'none' || css2 == 'none') {
			showLoader();
		}
	} else {
		hideLoader();
		$("body").show();
		$(".page-content").show();
		showTooltip('large');
		try {
			$(".relatedSkillsSeg").each(function() {
				try {
					var s = $(this).html().trim();
					if (s.endsWith(',')) {
						s = s.substring(0, s.length - 1);
						$(this).html(s);
					}
				} catch (e) {
					// TODO: handle exception
				}

			});
		} catch (e) {
			// TODO: handle exception
		}
	}
};
function showTooltip(size) {
	try {
		$(".link-bar-2").each(function() {
			var $this = $(this);
			$this.attr('readonly', '');
			if (this.offsetWidth < this.scrollWidth) {
				var addToolTip = true;
				$(this).siblings().each(function() {
					var className = $(this).attr("class");
					if (className == 'tooltip') {
						addToolTip = false;
					}
				});
				if (!addToolTip) {
					return;
				}
				if ($this.css('color').trim() == 'rgb(194, 34, 39)') {
					$this.css('color', '');
					$this.css('font-weight', '');
				}

				try {
					$this.css('cursor', 'pointer');
					$this.parent().attr('data-balloon-length', size);
					$this.parent().attr('data-balloon', $this.val());
					$this.parent().attr('data-balloon-pos', 'up');
				} catch (e) {
					// TODO: handle exception
				}
			}
		});
	} catch (e) {
		console.log("Error " + e);
	}
}