/**
 * 
 */

      function myFunction(div1,div2) {
        var checkBox = document.getElementById(div1);
        var text = document.getElementById(div2);
        if (checkBox.checked == true){
          text.style.display = "block";
        } else {
           text.style.display = "none";
        }
      }
   
$( "#urlTab" ).click(function() {
		        	$("#radioUrl"). prop("checked", true);
		        	document.getElementById("stepUrl").style.display='block';
		        	document.getElementById("stepHtml").style.display='none';
		        	});
		        $( "#htmlTab" ).click(function() {
		        	$("#radiohtml"). prop("checked", true);
		        	document.getElementById("stepUrl").style.display='none';
		        	document.getElementById("stepHtml").style.display='block';
});
		        
 $("input[name='optionsRadios']").change(function(){
            var valSel=$(this).val();
            if(valSel==='url'){
            	document.getElementById("urlMapped").style.display='block';
            	document.getElementById("htmlMapped").style.display='none';
            	document.getElementById("stepUrl").style.display='block';
            	document.getElementById("stepHtml").style.display='none';
            }else if(valSel==="html"){
            	document.getElementById("urlMapped").style.display='none';
            	document.getElementById("htmlMapped").style.display='block';
            	document.getElementById("stepUrl").style.display='none';
            	document.getElementById("stepHtml").style.display='block';
            }
        });
    
      /*  $('#checkbox1').change(function() {
            if($(this).is(":checked")) {
            	 document.getElementById("proSite").style.display='block';
            }else{
            	 document.getElementById("proSite").style.display='none';
          	 }
            });*/
        $("#submitScriptUrl").validationEngine('attach', { maxErrorsPerField:1});
  		jQuery("#submitScriptUrl").validationEngine();
        $("#submitUrl").click(function()
          {
        	if ($("#submitScriptUrl").validationEngine('validate')) 
			{
        		 var form = $('#submitScriptUrl');
            	 $("#loader").fadeIn();
            	 	$.ajax({
        				type : form.attr('method'),
        				url : form.attr('action'),
        				data : form.serialize(),
        				success : function(data) 
        				{
        					var d= $.trim(data.split("\n")[0]);
        					if(d==''){
        						$("#loader").hide();
        						//alert("No field fetched");
        						$("#errorText").empty();
        						$("#errorText").text('No field fetched')
        						$("#error-container").fadeIn();
        					}else{
        						$("#loader").hide();
        						document.getElementById("fieldsData").innerHTML=d;
        						document.getElementById("step1").style.display='none';
            	             	$("#step2").fadeIn(2000);
            	             	document.getElementById("stepActive").innerHTML="2";
            	             	var u=$("#serviceURL").val().trim();
            	             	$("#socialRedirectUrl").val(u);
            	             	$('#webFrame').attr('src', u);
            				}
            					$("#loader").hide();
        				}
        			}); 
				}
              });
        $("#submitScriptHtml").validationEngine('attach', { maxErrorsPerField:1});
 		 jQuery("#submitScriptHtml").validationEngine();
        $("#submitHtml").click(function()
           {
        	if ($("#submitScriptHtml").validationEngine('validate')) 
			{
        		 var form = $('#submitScriptHtml');
            	 $("#loader").fadeIn();
            	 	$.ajax({
        				type : form.attr('method'),
        				url : form.attr('action'),
        				data : form.serialize(),
        				success : function(data) 
        				{
        					var d= $.trim(data.split("\n")[0]);
        					if(d==''){
        						$("#loader").hide();
        					//	alert("No field fetched");
        						$("#errorText").empty();
        						$("#errorText").text('No field fetched');
        						$("#error-container").fadeIn();
        					}else{ 
	    						var u=$("#htmlSiteUrl").val();
	        	             	$("#socialRedirectUrl").val(u);
	        					document.getElementById("fieldsData").innerHTML=d;
	        					/* $("#loader").hide(); */
	        					document.getElementById("step1").style.display='none';
	        	             	$("#step2").fadeIn(2000);
	        	             	document.getElementById("stepActive").innerHTML="2";
	        	             	var ht=$("#htmlContent").val();
	        	             	var iframe = document.getElementById('webFrame');
								    iframedoc = iframe.contentDocument || iframe.contentWindow.document;
								iframedoc.body.innerHTML = ht;
								$("#loader").hide();
	        				}
	        					$("#loader").hide();
        				}
        			}); 
         	/* document.getElementById("step2").style.display='block';
         	document.getElementById("step1").style.display='none'; */
			}
            });
        $("#viewStep3").click(function()
                {
        	var c=0;
        	 $("#loader").fadeIn();
        	var fields=$("#formFieldList").val();var c=0;
        	var spliValFirlds=fields.split("##");
        	 for (var i = 0; i < spliValFirlds.length; i++) {
        	       var t=spliValFirlds[i].substring(0,spliValFirlds[i].indexOf("__"));
        	       if(t!=''){
					var ele= '#'+t;
        	       var x= $("ele").val();
        	      // console.log("X Value="+x)
						if(x!='Select Field'){
							c=1;
						}
        	       }
        	 }
        	 $("#loader").fadeOut();
        	 //console.log("C Value="+c)
				if(c==0){
					$("#alertMsg").fadeIn();
				}else{
					document.getElementById("stepApply").style.display='block';
	              	document.getElementById("step2").style.display='none';
	              	document.getElementById("stepActive").innerHTML="3";
	              	$("#outerDesign").removeClass( "col-md-12" ).addClass( "col-md-6" );
				}
        	//alert("step 3");
             });
        $("#submitFieldsForm").validationEngine('attach', { maxErrorsPerField:1});
		 jQuery("#submitFieldsForm").validationEngine();
        $("#submitFields").click(function()
        {
        	if(!($("#desktopChk").is(":checked") || $("#oneDriveChk").is(":checked")|| $("#googleDriveChk").is(":checked")|| $("#dropBoxChk").is(":checked")|| $("#linkedInChk").is(":checked"))){
        		$("#errorText").empty();
        		$("#errorText").text('Select atleast one option');
        		$("#error-container").fadeIn();
        	}else{
        		if ($("#submitFieldsForm").validationEngine('validate')) 
    			{
        	 var form = $('#submitFieldsForm');
        	 var url1=$("#serviceUrl1").val();
        	 var hml=$("#htmlContent").val();
        	 hml=hml.replace("&gt;", ">").replace("&lt;", "<");
        	 var clientName = $('#ClientNameSession').val().trim();
        	 $("#loader").fadeIn();
        	 	$.ajax({
    				type : form.attr('method'),
    				url : form.attr('action'),
    				data : form.serialize(),
    				success : function(data) 
    				{
    					var d= $.trim(data.split("\n")[0]);
    					
    					if(d!=''){
    						var dx='';
    						
    						if($('#dropBoxChk').is(":checked")){
    							var dk=$('#dropboxKey').val().trim();
    							dx='<script type="text/javascript" src="https://www.dropbox.com/static/api/2/dropins.js" id="dropboxjs" data-app-key="'+dk+'"></script>';
    						}
    						var c="<!-- Start of RScript -->";
    						var c1="<!-- End of RScript -->";
    					 
    					document.getElementById("scriptMessage").innerHTML=c+""+dx+""+d+""+c1;
    					var sl="/";
    					var msg="Hello \n\nWe have signed up for RChilli Solutions and would like to add the RScript Code to our website.\n\n";
    					msg=msg+"Please embed the code below to our website's HTML source code before closing of body tag </body>\n";
    					msg=msg+"\nRScript Code\n\n "+c+"\n"+dx+"\n"+d+"\n"+c1+"\n\nThanks,\n"+clientName;
    					document.getElementById('devMsg').value='';
    					$('#devMsg').val(msg);
    					$('#scriptTagVal').val(d);
    					var m="<input type=\"hidden\" value=\""+d+"\" id=\"scriptVal\" name=\"scriptVal\">";
    					document.getElementById("scriptText").innerHTML=m;
    					document.getElementById("stepApply").style.display='none';
		             	$("#step4").fadeIn(2000);
		             	document.getElementById("stepActive").innerHTML="4";
		             	var radioValue = $("input[name='optionsRadios']:checked").val();
		                 if(radioValue=='url'){
		                		$.ajax({
			            	 		type : "POST",
									url : "../rscript/checkhtmlUsed",
									data : 'Url='+url1+"&type=1&scriptTag="+dx+""+d,
									success : function(data) {
										html=$.trim(data.split("\n")[0]);
										var dataH=html.replace(/\-->/g,"--&gt;").replace(/\<!/g,"&lt;!").replace(/\</g,"&lt;").replace(/\>/g,"&gt;");
										dataH=dataH.replace("&lt;boldTag&gt;","<br><b>").replace("&lt;/boldTag&gt;","</b><br>");
										dataH=dataH.replace("&lt;comment&gt;","<br>").replace("&lt;/comment&gt;","<br>");
										dataH=dataH.replace("&lt;valid&gt;","<br>");
										document.getElementById("exampleText").innerHTML=dataH;
									}
			        		});
		                 }else if(radioValue='html'){
		                	 var returnString="";
		                	 /*test*/
		     				var html=hml.replace("&nbsp;", "").replace("\t", "");
		     				var bodyTag=html.substring( html.lastIndexOf("</body>"), html.length).split("\n");
		     				html=html.substring(0, html.lastIndexOf("</body>"));
		     				var xa=html.replace(" ", "").split("\n");
		     				var i=0;
		     				var y;
		     				for (y=xa.length-1; y>0;y--){
		     					if(i<5){
		     						if(xa[y].length>0){
		     							if(xa[y].length>50){
		     								returnString=xa[y].trim().substring(0, 50)+"....."+"\n"+returnString;
		     							}else{
		     								returnString=xa[y].trim()+"\n"+returnString;
		     							}
		     						}
		     					}else{
		     						break;
		     					}
		     					i++;
		     				}
		     				 
		     				returnString=returnString+"<boldTag><!-- Start of RScript --><comment>"+dx+""+d+"</comment><!-- End of RScript --></boldTag>\n";
		     				var x=0;var y;
		     				for (y=0;y<bodyTag.length;y++){
		     					if(x<=3){
		     						if(bodyTag[y].length>0){
		     							if(bodyTag[y].length>50){
		     								returnString=returnString+bodyTag[y].substring(0, 50)+"....."+"\n";
		     							}else{
		     								returnString=returnString+bodyTag[y]+"\n";
		     							}
		     						}
		     					}else{
		     						break;
		     					}
		     					i++;
		     			}
		     				returnString=returnString.replace("-->","--&gt;").replace("<!","&lt;!").replace("<","&lt;").replace(">","&gt;");
		                	 
		                	 var dataH=returnString.replace(/\-->/g,"--&gt;").replace(/\<!/g,"&lt;!").replace(/\</g,"&lt;").replace(/\>/g,"&gt;");
								dataH=dataH.replace("&lt;boldTag&gt;","<br><b>").replace("&lt;/boldTag&gt;","</b><br>");
								dataH=dataH.replace("&lt;comment&gt;","<br>").replace("&lt;/comment&gt;","<br>");
								document.getElementById("exampleText").innerHTML=dataH; 
		                	 }
		             	
    	             	$("#loader").hide();
    				}else{
    					$("#loader").hide();
    				}
    				}
    			}); 
    			}
                }
                });
     
        $('#viewMail').change(function() {
            if($(this).is(":checked")) {
            	 document.getElementById("mailBlock").style.display='block';
            }else{
            	 document.getElementById("mailBlock").style.display='none';
          	 }
            });
     
        $('#scriptConfirm').change(function() {
            if($(this).is(":checked")) {
            	$("#viewStep4" ).prop( "disabled", false );
            	$('#viewStep4').css('border','0.5em solid #c22227');
            	$('#viewStep4').css('background-color','#c22227');
            }else{
            	$("#viewStep4" ).prop( "disabled", true );
            	$('#viewStep4').css('border','0.5em solid #E17D67');
            	$('#viewStep4').css('background-color','#E17D67');
          	 }
            });
        $("#viewStep32").click(function() {
              	document.getElementById("step4").style.display='block';
              	document.getElementById("step5").style.display='none';
              	document.getElementById("stepActive").innerHTML="4";
              	
        });
        $("#viewStep1").click(function() {
        		//alert("step 31");
        		document.getElementById("step1").style.display='block';
              	document.getElementById("step2").style.display='none';
              	document.getElementById("step4").style.display='none';
              	 var radioValue = $("input[name='optionsRadios']:checked").val();
                 if(radioValue=='url'){
                	 document.getElementById("stepUrl").style.display='block';
                 }else{
                	 if(radioValue='html'){
                		 document.getElementById("stepHtml").style.display='block';
                	 }
                 }
              	document.getElementById("stepActive").innerHTML="1";
        });
        $("#viewStep2").click(function() {
              	document.getElementById("step2").style.display='block';
              	document.getElementById("stepApply").style.display='none';
                document.getElementById("stepActive").innerHTML="2";
        });
        $("#viewStepApply").click(function()   {
              	document.getElementById("step4").style.display='none';
              	document.getElementById("stepApply").style.display='block';
              	document.getElementById("stepActive").innerHTML="3";              	
        });
        $("#viewStep4").click(function()  {
        		var url1=$("#serviceUrl1").val();
        		var plug=$('#scriptTagVal').val();
        		var x=plug.split("com/");
				var scriptNo=x[1].substring(0,x[1].indexOf(".js"));
				var radioValue = $("input[name='optionsRadios']:checked").val();
				var test="";
                if(radioValue=='url'){
        			$("#loader").fadeIn();
            	 	$.ajax({
            	 		type : "POST",
						url : "../rscript/checkhtmlUsed",
						data : 'Url='+url1+"&type=0",
						async:false,
						success : function(data) {
							$("#loader").hide();
							html=$.trim(data.split("\n")[0]);
							var hData=html.replace("--&gt;","-->").replace("&lt;!","<!").replace("&lt;","<").replace("&gt;",">");
							if(hData.indexOf(plug)>0){
        						$( "#chkJSParsing" ).prop( "checked", true );
        						test="first";
        					}
						}
            	 	});
        		}else{
        			$.ajax({
						type : "POST",
						url : "../rscript/checkScriptUsed",
						data : 'scriptNo='+scriptNo,
						async:false,
						success : function(data) {
							html=$.trim(data.split("\n")[0]);
							$("#loader").hide();
							if (html.trim() != "") {
								$( "#chkResumeParsing" ).prop( "checked", true );
								if(test=="first"){
									test="done";
								}
							}
						}
					});
        		}
        		 if($('#chkJSParsing').is(":checked") && $('#chkResumeParsing').is(":checked")) {
                   	 document.getElementById("finish").style.display='block';
                  }
        		//Code for checking if script placed on page or not in case of url entered
        		//Code to check if has processed one resume or not
              	document.getElementById("step5").style.display='block';
              	document.getElementById("step4").style.display='none';
              	document.getElementById("stepActive").innerHTML="5";
          });
      
        $("#submitDevMail").validationEngine('attach', { maxErrorsPerField:1});
	   jQuery("#submitDevMail").validationEngine();
       $("#sendMail").click(function()
          {
       	if ($("#submitDevMail").validationEngine('validate')) 
			{
       		 var form = $('#submitDevMail');
           	 $("#loader").fadeIn();
           	 	$.ajax({
       				type : form.attr('method'),
       				url : form.attr('action'),
       				data : form.serialize(),
       				success : function(data) 
       				{
       					var d= $.trim(data.split("\n")[0]);
       					if(d!=''){
       						document.getElementById("sendMailMsg").innerHTML="Email successfully sent";
       						document.getElementById("sendMailMsg").style.display='block';
       						document.getElementById("notSendMail").style.display='none';
       					}else{
       						document.getElementById("notSendMail").innerHTML="Issue in sending mail";
       						document.getElementById("notSendMail").style.display='block';
       						document.getElementById("sendMailMsg").style.display='none';
       					}
       	             	$("#loader").hide();
       				}
       			}); 
        	/* document.getElementById("step2").style.display='block';
        	document.getElementById("step1").style.display='none'; */
			}
           });
       $('#chkJSParsing').change(function() {
           if($(this).is(":checked") && $('#chkResumeParsing').is(":checked")) {
	           	$("#finish" ).prop( "disabled", false );
	        	$('#finish').css('border','0.5em solid #c22227');
	        	$('#finish').css('background-color','#c22227');
           }else{
        		$("#finish" ).prop( "disabled", true );
        		$('#finish').css('border','0.5em solid #E17D67');
        		$('#finish').css('background-color','#E17D67');
           }
       });
       $('#chkResumeParsing').change(function() {
           if($(this).is(":checked") && $('#chkJSParsing').is(":checked")) {
        	   $("#finish" ).prop( "disabled", false );
	        	$('#finish').css('border','0.5em solid #c22227');
	        	$('#finish').css('background-color','#c22227');
           }else{
        		$("#finish" ).prop( "disabled", true );
        		$('#finish').css('border','0.5em solid #E17D67');
        		$('#finish').css('background-color','#E17D67');
           }
        });
       $("#finish").click(function()
	   	 {
    	   var seconds = 3;
           $("#success-container").fadeIn();
           setInterval(function () {
               seconds--;
               if (seconds == 0) {
            	   $("#success-container").fadeOut();
                   window.location = "/demo/rscript";
               }
           }, 1000);
	     });
       $(".delRscript").click(function() {
    	   var conf = confirm("Are you sure you want to delete this script?");
         	if(conf == true)
         	{
    		var pId=this.id; 
    		pId=pId.replace("pId","")
    		var dataString = 'pId=' + pId;
    	   $.ajax({
				type : "POST",
				url : "../rscript/deletePlugin",
				data : dataString,
				cache : false,
				success : function(data) {
					html= $.trim(data.split("\n")[0]);
					if (html.trim() == "success") {
						$('#'+pId).remove();
					}else{
						alert("Unable to delete plugin");
					}
				}
			});
         	}
         	}); 
       $('.fieldMapSelect').change(function() {    
    	    var item=$(this);
    	    if(item.val()=='Select Field'){
    	    	$($(this)).css("border-color", "red");
    	    }else{
    	    	$($(this)).css("border-color", "green");
    	    }
    	});
       $(document).ready(function() {
    	   
    		$("#alertMsgClose").click(function()
			{
				$("#alertMsg").fadeOut();
			});
    		$("#alertSocialMsgClose").click(function()
			{
				$("#alertSocialMsg").fadeOut();
			});
    		$("#alertFinishMsgClose").click(function()
			{
				$("#alertFinishMsg").fadeOut();
			});
       $(".addFieldButton").click(function(e){ 
    	   var a="<input type=\"text\" placeholder=\"Id or name of field\"><select ><option>Id</option><option>Name</option></select>";
    	   //alert(a);
    	   document.getElementById('fieldsData1').innerHTML += a;
 	          });
       
       $("#radioUrl").click(function()
       {
    		var outerDesignHeight = $("#outerDesign").height();
    		$("#stepHtml").css("height", outerDesignHeight+"css");
       });
       
       });
       $(document).on('click', '.fieldMapSelect', function () {    	   
    	 var item=$(this);
	    if(item.val()=='Select Field'){
	    	$($(this)).css("border-color", "red");
	    }else{
	    	$($(this)).css("border-color", "green");
	    }
	});
       

       function closeError() {
       	$('#error-container').hide(200);
       }
       function closeSuccess() {
       	$('#success-container').hide(200);
       }
       function showSuccess() {
       	hideLoader();
       	$('#success-container').show(200);
       }
       function showError() {
       	hideLoader();
       	$('#error-container').show(200);
       }
       
       $(document).on("change" , "#buttonBorderColorPicker" , function(){
    	   	 var hex=$(this).val();
    	     $("#previewApplyNow").css("border-color", hex);
    	     $("#buttonBorderColor").val(hex);
       });
       
       $(document).on("change" , "#buttonColorPicker" , function(){
    	   	 var hex=$(this).val();
    	     $("#previewApplyNow").css("background-color", '#' + hex);
    	     $("#buttonColor").val(hex);
       });
       $(document).on("change" , "#dropdownBackColorPicker" , function(){
  //     $('#dropdownBackColorPicker').ColorPicker({
    //	   onChange: function(hsb, hex, rgb){
    	   	 var hex=$(this).val();
    	     //$("#previewApplyNow").css("background-color", '#' + hex);
    	     $("#dropdownBackColor").val(hex);
    	//   }
       });
       $(document).on("change" , "#buttonTextColorPicker" , function(){
  //     $('#buttonTextColorPicker').ColorPicker({
  // 	   onChange: function(hsb, hex, rgb){
    	   	 var hex=$(this).val();
    	     $("#previewApplyNow").css("color", '#' + hex);
    	     $("#buttonTextColor").val(hex);
    //	   }
       });
       
       function myFunction1() {
   	    document.getElementById("myDropdown").classList.toggle("show");
   	}

$(document).ready(function() {
	$('#rscript').addClass('active-menu'); 
});
       