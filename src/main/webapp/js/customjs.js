function openData(evt, cityName) {
	var i, tabcontent, tablinks;
	tabcontent = document.getElementsByClassName("data-box");
	for (i = 0; i < tabcontent.length; i++) {
		tabcontent[i].style.display = "none";
	}
	tablinks = document.getElementsByClassName("data-link");
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(" active", "");
	}
	document.getElementById(cityName).style.display = "block";

	if (cityName === 'parse') {
		document.getElementById("defaultOpenParse").click();
	} else if (cityName === 'json') {
		document.getElementById("defaultOpenJson").click();
	} else if (cityName.includes('parse')) {
		let i = 1;
		while (i <= 50) {
			if (cityName === 'parse' + i) {
				document.getElementById("defaultOpenParse" + i).click();
				break;
			}
			i++;
		}
	} else if (cityName.includes('json')) {
		let i = 1;
		while (i <= 50) {
			if (cityName === 'json' + i) {
				document.getElementById("defaultOpenJson" + i).click();
				break;
			}
			i++;
		}
	}
	if (cityName == 'smb') {
		openTab(this, 'smbYearly');
	} else if (cityName == 'cloud') {
		openTab(this, 'cloudYearly')
	}

}
$(document).ready(function() {
	try {
		var raw = $('.rawJSONResumeParser').val();
		var jsonobj = JSON.parse(raw);
		$('.rawJSONResumeParser').val(JSON.stringify(jsonobj, null, '\t'));
	} catch (err) {
		// console.log("error" + err);
	}
	$("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
		e.preventDefault();
		$(this).siblings('a.active').removeClass("active");
		$(this).addClass("active");
		var index = $(this).index();
		$("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
		$("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
	});

});

function closeError() {
	$('#error-container').hide(200);
}
function closeAlert() {
	$('#alert-container').hide(200);
}
function closeSuccess() {
	$('#success-container').hide(200);
}
function showSuccess() {
	hideLoader();
	$('#success-container').show(200);
}
function showError() {
	hideLoader();
	$('#error-container').show(200);
}
function showAlert() {
	hideLoader();
	$('#alert-container').show(200);
}
function showLoader() {
	$('#loader').show(200);
}
function hideLoader() {
	$('#loader').hide(200);
}
function setErrorText(x) {
	$('#errorText').html(x);
}
function setSuccessText(x) {
	$('#successText').html(x);
}
function setAlertText(x) {
	$('#alertText').html(x);
}
function closePlanExpirepopUp() {
	$('#plan-expire-pop-up').hide(200);
	$.ajax({
		url : "addAlertCookie",
		method : "GET",
		success : function(result) {

		},
		error : function(error) {
			// console.log(error);
		}

	});

}
$(document).on("click", "#downloadJson, .downloadJson", function() {
	try {
		showLoader();
		var data = $(".rawJSONResumeParser").val();
		var fileName = $('#fileName').val();
		fileName = fileName.substring(0, fileName.lastIndexOf("."));
		download(data, fileName + ".json", "application/octet-stream");
		hideLoader();
	} catch (e) {
		hideLoader();
		// console.log(e);
	}
});
function downloadJSON(i) {
	try {
		showLoader();
		var data = $('#rawJSON' + i).val();
		var fileName = $('#fileName' + i).val();
		fileName = fileName.substring(0, fileName.lastIndexOf("."));
		download(data, fileName + ".json", "application/octet-stream");
		hideLoader();
	} catch (e) {
		hideLoader();
		console.log(e);
	}
}

function openTab(evt, blockName) {
	try {
		var i, tabcontent, tablinks;
		tabcontent = document.getElementsByClassName("tabcontent");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(blockName).style.display = "block";
		evt.currentTarget.className += " active";
	} catch (e) {
		// console.log('error :' + e);
	}
	try {
		if (blockName.includes('cloud') || blockName.includes('smb')) {
			$('.radio').each(function() {
				try {
					$(this).prop("checked", false);
				} catch (e) {
					// TODO: handle exception
				}
			});
			$('#' + blockName + 'Input').prop("checked", true);
			$('.leftArrow').show();
			$('.rightArrow').show();
		}
	} catch (e) {
		// TODO: handle exception
	}
	showTooltip('large');
}
// document.getElementById("defaultOpen").click();
// Get the element with id="defaultOpen" and click on it
// document.getElementById("defaultOpen").click();

function openCity(evt, cityName) {
	var i, tabcontent, tablinks;
	tabcontent = document.getElementsByClassName("fil-content");
	for (i = 0; i < tabcontent.length; i++) {
		tabcontent[i].style.display = "none";
	}
	tablinks = document.getElementsByClassName("filter-link");
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(" active", "");
	}
	document.getElementById(cityName).style.display = "block";
	evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
// document.getElementById("boxing-1").click();

/*
 * function opendata(evt, cityName) { var i, box-data, open-btn; box-data =
 * document.getElementsByClassName("box-data"); for (i = 0; i < box-data.length;
 * i++) { box-data[i].style.display = "none"; } open-btn =
 * document.getElementsByClassName("open-btn"); for (i = 0; i < open-btn.length;
 * i++) { open-btn[i].className = tablinks[i].className.replace(" active", ""); }
 * document.getElementById(cityName).style.display = "block";
 * evt.currentTarget.className += " active"; }
 */

$(".eyeIcon").click(function() {
	$(this).siblings().each(function() {
		var tx = $(this).attr('type');
		if (tx == 'password') {
			$(".eyeIcon").removeClass("fa-eye");
			$(".eyeIcon").addClass("fa-eye-slash");
			$(this).attr('type', 'text');
		} else {
			$(".eyeIcon").removeClass("fa-eye-slash");
			$(".eyeIcon").addClass("fa-eye");
			$(this).attr('type', 'password');
		}
	});
});

// function openData(evt, cityName) {
// var i, tabcontent, tablinks;
// tabcontent = document.getElementsByClassName("data-box");
// for (i = 0; i < tabcontent.length; i++) {
// tabcontent[i].style.display = "none";
// }
// tablinks = document.getElementsByClassName("data-link");
// for (i = 0; i < tablinks.length; i++) {
// tablinks[i].className = tablinks[i].className.replace(" active", "");
// }
// document.getElementById(cityName).style.display = "block";
// evt.currentTarget.className += " active";
// }

// // Get the element with id="defaultOpen" and click on it
// document.getElementById("defaultOpen").click();

// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal
// btn.onclick = function() {
// modal.style.display = "block";
// }

// When the user clicks on <span> (x), close the modal
// span.onclick = function() {
// modal.style.display = "none";
// }

// When the user clicks anywhere outside of the modal, close it

jQuery(function($) {
	$('.sidebar-dropdown > a').click(function() {
		$('.sidebar-submenu').slideUp(200);
		if ($(this).parent().hasClass('active')) {
			$('.sidebar-dropdown').removeClass('active');
			$(this).parent().removeClass('active');
		} else {
			$('.sidebar-dropdown').removeClass('active');
			$(this).next('.sidebar-submenu').slideDown(200);
			$(this).parent().addClass('active');
		}
	});

	$('#close-sidebar').click(function() {
		$('.page-wrapper').removeClass('toggled');
	});

	$('#show-sidebar').click(function() {
		if ($('.page-wrapper').hasClass('toggled')) {
			$('.page-wrapper').removeClass('toggled');
		} else {
			$('.page-wrapper').addClass('toggled');
		}
	});
	if (window.matchMedia('(max-width: 1024px)').matches) {
		$('.page-wrapper').removeClass('toggled');
	}

	// open
	$('#template1 .box').click(function() {
		var idElement = $(this).attr('data-section');

		$('#v-pills-home-tab').removeClass('active');
		$('#v-pills-ats-tab').removeClass('active');
		$('#v-pills-profile-tab').removeClass('active');
		$('#v-pills-database-tab').removeClass('active');
		$('#v-pills-messages-tab').removeClass('active');
		$('#v-pills-sreadsheet-tab').removeClass('active');
		$('#v-pills-seven-tab').removeClass('active');
		$('#v-pills-eight-tab').removeClass('active');

		$('#' + idElement).addClass('active');

		$('#template2').show();
		$('#template1').hide();
		$('.hide-on-modal').hide();
	});
	// close
	$('.close-tab .close-icon').click(function() {
		$('#template1').show(500);
		$('#template2').hide();
		$('.hide-on-modal').show();
	});
	$('#exampleModalCenter').modal('show');
	$('.show-hide-toggle').click(function() {
		$('.zapier-right').toggleClass('d-none');
	});

	// Active left menu

	$('.sidebar-menu li .active-left').click(function() {
		$('.sidebar-menu li .active-left').removeClass('active-menu');
		$(this).addClass('active-menu');
	});

	$('.usage-top-actions a').click(function() {
		$('.usage-top-actions a').removeClass('active');
		$(this).addClass('active');
	});

	$("select.form-control").change(function() {
		$(this).css('color', 'black')
	})
});

// js for vedio op up

$(document).ready(function() {
	/*
	 * Get iframe src attribute value i.e. YouTube video url and store it in a
	 * variable
	 */
	var url = $("#rchilliVedio").attr('src');

	/*
	 * Assign empty url value to the iframe src attribute when modal hide, which
	 * stop the video playing
	 */
	$("#myModal").on('hide.bs.modal', function() {
		$("#rchilliVedio").attr('src', '');
	});

	/*
	 * Assign the initially stored url back to the iframe src attribute when
	 * modal is displayed again
	 */
	$("#myModal").on('show.bs.modal', function() {
		$("#rchilliVedio").attr('src', url);
	});
});

function myFunction() {
	document.getElementById("vedio-pause").readOnly = true;
}

// Get the modal
var modal = document.getElementById("myModal");

/*
 * // Get the button that opens the modal var btn =
 * document.getElementById("myBtn"); // Get the <span> element that closes the
 * modal var span = document.getElementsByClassName("close")[0]; // When the
 * user clicks the button, open the modal btn.onclick = function() {
 * modal.style.display = "block"; } // When the user clicks on <span> (x), close
 * the modal span.onclick = function() { modal.style.display = "none"; }
 */

$(function() {
	$('#nav li a').click(function() {
		$('#nav li').removeClass();
		$($(this).attr('href')).addClass('active');
	});
});

// javascript for crousal

// var slideIndex = 1;
// showSlides(slideIndex);

// function plusSlides(n) {
// showSlides(slideIndex += n);
// }

// function currentSlide(n) {
// showSlides(slideIndex = n);
// }

// function showSlides(n) {
// var i;
// var slides = document.getElementsByClassName("mySlides");
// var dots = document.getElementsByClassName("dot");
// if (n > slides.length) {slideIndex = 1}
// if (n < 1) {slideIndex = slides.length}
// for (i = 0; i < slides.length; i++) {
// slides[i].style.display = "none";
// }
// for (i = 0; i < dots.length; i++) {
// dots[i].className = dots[i].className.replace(" active", "");
// }
// slides[slideIndex-1].style.display = "block";
// dots[slideIndex-1].className += " active";
// }

function download(data, strFileName, strMimeType) {
	var self = window, // this script is only for browsers anyway...
	u = "application/octet-stream", // this default mime also triggers iframe
	// downloads
	m = strMimeType || u, x = data, D = document, a = D.createElement("a"), z = function(a) {
		return String(a);
	},

	B = self.Blob || self.MozBlob || self.WebKitBlob || z, BB = self.MSBlobBuilder || self.WebKitBlobBuilder || self.BlobBuilder, fn = strFileName
			|| "download", blob, b, ua, fr;

	// if(typeof B.bind === 'function' ){ B=B.bind(self); }

	if (String(this) === "true") { // reverse arguments, allowing
		// download.bind(true, "text/xml",
		// "export.xml") to act as a callback
		x = [ x, m ];
		m = x[0];
		x = x[1];
	}

	// go ahead and download dataURLs right away
	if (String(x).match(/^data\:[\w+\-]+\/[\w+\-]+[,;]/)) {
		return navigator.msSaveBlob ? // IE10 can't do a[download], only
		// Blobs:
		navigator.msSaveBlob(d2b(x), fn) : saver(x); // everyone else can
		// save dataURLs
		// un-processed
	}// end if dataURL passed?

	try {

		blob = x instanceof B ? x : new B([ x ], {
			type : m
		});
	} catch (y) {
		if (BB) {
			b = new BB();
			b.append([ x ]);
			blob = b.getBlob(m); // the blob
		}

	}

	function d2b(u) {
		var p = u.split(/[:;,]/), t = p[1], dec = p[2] == "base64" ? atob : decodeURIComponent, bin = dec(p.pop()), mx = bin.length, i = 0, uia = new Uint8Array(
				mx);

		for (i; i < mx; ++i)
			uia[i] = bin.charCodeAt(i);

		return new B([ uia ], {
			type : t
		});
	}

	function saver(url, winMode) {

		if ('download' in a) { // html5 A[download]
			a.href = url;
			a.setAttribute("download", fn);
			a.innerHTML = "downloading...";
			D.body.appendChild(a);

			setTimeout(function() {
				a.click();
				D.body.removeChild(a);
				if (winMode === true) {
					setTimeout(function() {
						self.URL.revokeObjectURL(a.href);
					}, 250);
				}
			}, 66);
			return true;
		}

		// do iframe dataURL download (old ch+FF):
		var f = D.createElement("iframe");
		D.body.appendChild(f);
		if (!winMode) { // force a mime that will download:
			url = "data:" + url.replace(/^data:([\w\/\-\+]+)/, u);
		}

		f.src = url;

		setTimeout(function() {
			D.body.removeChild(f);
		}, 333);

	}// end saver

	if (navigator.msSaveBlob) { // IE10+ : (has Blob, but not a[download] or
		// URL)
		return navigator.msSaveBlob(blob, fn);
	}

	if (self.URL) { // simple fast and modern way using Blob and URL:
		saver(self.URL.createObjectURL(blob), true);
	} else {
		// handle non-Blob()+non-URL browsers:
		if (typeof blob === "string" || blob.constructor === z) {
			try {

				return saver("data:" + m + ";base64," + self.btoa(blob));
			} catch (y) {

				return saver("data:" + m + "," + encodeURIComponent(blob));
			}
		}

		// Blob but not URL:
		fr = new FileReader();
		fr.onload = function(e) {
			saver(this.result);
		};
		fr.readAsDataURL(blob);
	}
	return true;
}

$('.MoreDescription').click(function() {
	var $text = $(this).prev(".text-block");
	if ($text.css("-webkit-line-clamp") === "10000") {
		$text.css("-webkit-line-clamp", "3");
		$(this).html("Read More");
	} else {
		$text.css("-webkit-line-clamp", "10000");
		$(this).html("Read Less");
	}
});

function changeType(elementId, currentElement) {

	try {
		var type = $('#' + elementId).attr("type");
		if (type === 'password') {
			$('#' + elementId).attr("type", "text");
			$(currentElement).addClass('fa-eye');
			$(currentElement).removeClass('fa-eye-slash');
		} else if (type === 'text') {
			$('#' + elementId).attr("type", "password");
			$(currentElement).removeClass('fa-eye');
			$(currentElement).addClass('fa-eye-slash');
		}
	} catch (e) {
		// console.log("Error : " + e);
	}
}
function changeType2(elementId, currentElement) {

	try {
		var type = $('#' + elementId).css("-webkit-text-security");
		if (type === 'disc') {
			$('#' + elementId).css("-webkit-text-security", "none");
			$(currentElement).addClass('fa-eye');
			$(currentElement).removeClass('fa-eye-slash');
		} else if (type === 'none') {
			$('#' + elementId).css("-webkit-text-security", "disc");
			$(currentElement).removeClass('fa-eye');
			$(currentElement).addClass('fa-eye-slash');
		}
	} catch (e) {
		// console.log("Error : " + e);
	}
}
function enterToSearch(event, getTextFromId, searchTextFromId) {
	var key = event.keyCode;
	if (key == 13) {
		search(getTextFromId, searchTextFromId);
	}

}
/*
 * function search1(getTextFromId, searchTextFromId) {
 * console.log(getTextFromId+" : "+searchTextFromId); var searchTxtBox = $('#' +
 * getTextFromId); searchTxtBox.val(searchTxtBox.val().replace(/(\s+)/, "(<[^>]+>)*$1(<[^>]+>)*"));
 * var textarea = $('.' + searchTextFromId); var enew = ''; if
 * (searchTxtBox.val() != '') { enew = textarea.html().replace(/(<mark
 * id=\"test\">|<\/mark>)/igm, "");
 * 
 * let i = 1; while (i <= 50) { if ($('.jsonOutput' + i).length > 0 &&
 * searchTextFromId !== 'jsonOutput' + i) { // var x =
 * $('.jsonOutput'+i).html().replace(/(<mark // id=\"test\">|<\/mark>)/igm,
 * ""); // $('.jsonOutput'+i).html(x); } i++; }
 * 
 * textarea.html(enew); var query = new RegExp("(" + searchTxtBox.val() + ")",
 * "gim"); newtext = textarea.html().replace(query, "<mark id=\"test\">$1</mark>");
 * newtext = newtext.replace(/(<mark id=\"test\">[^<>]*)((<[^>]+>)+)([^<>]*<\/mark>)/, "</mark><mark>");
 * textarea.html(newtext); $("." + searchTextFromId).animate({ scrollTop :
 * $("#test").offset().top - 600 }, 2000); } }
 */
function search(textToSearchID, searchFromID) {
	try {
		var textToSearch = $('#' + textToSearchID).val();
		textToSearch = textToSearch.replace(/(\s+)/, "(<[^>]+>)*$1(<[^>]+>)*");
		if (textToSearch != '') {
			// showLoader();
			let i = 1;
			while (i <= 50) {
				if ($('.jsonOutput' + i).length > 0) {
					if ($('.jsonOutput' + i).length > 0 && textToSearchID !== 'jsonOutput' + i) {
						var x = $('.jsonOutput' + i).html().replace(/(<mark id=\"test\">|<\/mark>)/igm, "");
						$('.jsonOutput' + i).html(x);
					}
				}
				i++;
			}
			$("." + searchFromID).find("*").each(function() {
				if (!$(this).html().includes("<div") && !$(this).html().includes("<span") && !$(this).html().includes("<li")) {
					$(this).html(search2(textToSearch, $(this).html()));
				}
			});
			$("." + searchFromID).scrollTo(document.getElementById("test"), 1600, {
				queue : true
			});
			// $("." + searchFromID).scrollTo($("#test"));
			/*
			 * $("." + searchFromID).animate({ 'scrollTop' :
			 * $("#test").position().top - 600 }, 2000); $("." +
			 * searchFromID).animate({ 'scrollRight' :
			 * $("#test").position().right - 200 }, 2000);
			 */
		}
	} catch (e) {
		console.log("Error" + e);
	}
	hideLoader();
}
function search2(textToSearch, searchFrom) {
	searchFrom = searchFrom.replace(/(<mark id=\"test\">|<\/mark>)/igm, "");
	var query = new RegExp("(" + textToSearch + ")", "gim");
	newtext = searchFrom.replace(query, "<mark id=\"test\">$1</mark>");
	newtext = newtext.replace(/(<mark id=\"test\">[^<>]*)((<[^>]+>)+)([^<>]*<\/mark>)/, "</mark><mark>");
	return newtext;
}
function closeModal() {
	$(".modal").hide();
}
$(document).on("click", "#show-sidebar", function() {
	$('#show-sidebar').css('display', 'none');
	$('#show-sidebar-custom').css('display', 'block');
});
$(document).on("click", "#show-sidebar-custom", function() {
	$('.page-wrapper').removeClass('toggled');
	$('#show-sidebar').css('display', 'block');
	$('#show-sidebar-custom').css('display', 'none');
});
function checkPlanExpire(planExpiryMsg) {
	try {
		if (planExpiryMsg != '') {
			$('#plan-expire-pop-up').show(200);
			$('#planExpiryMsg').html(planExpiryMsg);
		}
	} catch (e) {
		console.log(e);
	}

}
function capitalizeFirstLetter(string) {
	try{
		string = string.trim();
		return string.charAt(0).toUpperCase() + string.slice(1);
	}catch (e) {
		return string;
	}
}
