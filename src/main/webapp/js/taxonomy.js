var URL = '';
var version = '';
var key = '';
var language = '';
var APILocale = '';
var customValues = '';

function setTaxonomyInput(URL, version, key, language, APILocale, customValues){
	this.URL = URL;
	this.version = version;
	this.key = key;
	this.language = language;
	this.APILocale = APILocale;
	this.customValues = customValues;
}

$("#btnSkill").click(function()
	{
		var searchText = $("#txtSkill").val();
		getSkillDetail(searchText);
	});

	$('#txtSkill').keydown(function(event){    
	    if(event.keyCode==13){
	    	var searchText = $("#txtSkill").val();
			getSkillDetail(searchText);
	    }
	});
	
	//get skill detail in job profile block
	$(document).on("click", ".jobProfileSkills, .skillRelatedSkills", function()
	{
		try{
			var searchText = $(this).val();
			getSkillDetail(searchText);
			console.log('here');
			$("#open-result-jobprofile").removeClass("active");
			$("#open-result").addClass("active");
			$("#defaultOpenSkill").addClass("defaultOpenSkill");
			document.getElementById("defaultOpenSkill").click();
			document.getElementById("open-result").click();
		}catch (e) {
			console.log('error ::'+e);
		}
	});
	
	function getSkillDetail(searchText)
	{
		
		$("#apiErrorMessage").hide();
		$("#skillErrorMessage").hide();
		try
		{
			if(searchText != null && searchText != "")
			{
				$("#loader").show();
				var searchInUrl = URL+"skillsearch";
				var jsonData = { ApiKey : key, Version : version, Language : language, Locale : APILocale, CustomValues : customValues, Keyword : ""+searchText+""};
				
				$.ajax({
	                    type: "POST",
	                    url: searchInUrl,
	                    data:  JSON.stringify(jsonData),
	                    contentType: "application/json", 
	                    dataType: "json",
	                    success: function (response) {
	                    hideLoader();
		                var respData= JSON.stringify(response, undefined, 4);
		                if(respData != null && respData != "")
						{
							if(respData.indexOf("errorcode") == -1)
							{
								$('.outputSkillDiv').show();
								var obj = JSON.parse(respData);
								if(obj.Skill.SkillData.FormattedSkill != null && obj.Skill.SkillData.FormattedSkill != "")
								{
									$("#searchSkill").val(obj.Skill.SearchKeyword);
									$("#formattedSkill").val(obj.Skill.SkillData.FormattedSkill);
									$("#skillType").val(obj.Skill.SkillData.SkillType);
									$("#skillDescription").text(obj.Skill.SkillData.Description);
									try{
										openTab(event, 'RawJsonOutput');
										$('#defaultOpenSkill').addClass('active');
									}catch (e) {
										// TODO: handle exception
									}
									try
									{
										//$("#skillAlias").html(obj.Skill.SkillData.Alias);
										/*var skillsC = '';
										for (var i = 0; i < obj.Skill.SkillData.Alias.length.length; i++) {
											if(i !==0)
											{
												skillsC = skillsC +", ";
											}
											skillsC += cars[i];
										}*/
										var skillsC = '';
										
										$.each(obj.Skill.SkillData.Alias, function(index, val) { 
											if(index !==0)
											{
												skillsC = skillsC +", ";
											}
											skillsC = skillsC+val;
										});
										$("#skillAlias").html(skillsC);
									}
									catch(err)
									{
										console.log(err);
									}
	
									try
									{
										var relatedSkills = "";
										$.each(obj.Skill.SkillData.RelatedSkills, function(key,value) 
										{
											relatedSkills = relatedSkills+'<div class="col-md-4 ">';
											relatedSkills = relatedSkills+'<label class=" pt-3 lable-form">Skill</label>';
											relatedSkills = relatedSkills+'<div class="form-group">';
											relatedSkills = relatedSkills+'<input type="text" value="'+value.Skill+'" readonly="" class="link-bar-3 w-100 skillRelatedSkills" placeholder="">';
											relatedSkills = relatedSkills+'</div>';
											relatedSkills = relatedSkills+'</div>';
											relatedSkills = relatedSkills+'<div class="col-md-4 ">';
											relatedSkills = relatedSkills+'<label class=" pt-3 lable-form">Relation</label>';
											relatedSkills = relatedSkills+'<div class="form-group">';
											relatedSkills = relatedSkills+'<input type="text" value="'+value.Relation+'" class="link-bar-2 w-100" placeholder="">';
											relatedSkills = relatedSkills+'</div>';
											relatedSkills = relatedSkills+'</div>';
											relatedSkills = relatedSkills+'<div class="col-md-4 ">';
											relatedSkills = relatedSkills+'<label class=" pt-3 lable-form">Skill Type</label>';
											relatedSkills = relatedSkills+'<div class="form-group">';
											relatedSkills = relatedSkills+'<input type="text" value="'+value.Type+'" class="link-bar-2 w-100" placeholder="">';
											relatedSkills = relatedSkills+'</div>';
											relatedSkills = relatedSkills+'</div>';
										}); 
										$("#relatedSkillsBlock").html(relatedSkills);
									}
									catch(err)
									{}
	
									try
									{
										var relatedJobProfiles = "";
										$.each(obj.Skill.SkillData.RelatedJobProfile, function(key,value) 
										{
											
											
											relatedJobProfiles = relatedJobProfiles+'<div class="col-md-5 ">';
											relatedJobProfiles = relatedJobProfiles+'<label class=" pt-3 lable-form">Job Profile</label>';
											relatedJobProfiles = relatedJobProfiles+'<div class="form-group">';
											relatedJobProfiles = relatedJobProfiles+'<input type="text" value="'+value.JobProfile+'" class="link-bar-3 w-100 skillRelatedJobProfile" placeholder="">';
											relatedJobProfiles = relatedJobProfiles+'</div></div>';
											relatedJobProfiles = relatedJobProfiles+'<div class="col-md-5 ">';
											relatedJobProfiles = relatedJobProfiles+'<label class=" pt-3 lable-form">Proficiency Level</label>';
											relatedJobProfiles = relatedJobProfiles+'<div class="form-group">';
											relatedJobProfiles = relatedJobProfiles+'<input type="text" value="'+value.ProficiencyLevel+'" class="link-bar-2 w-100" placeholder="">';
											relatedJobProfiles = relatedJobProfiles+'</div></div>';
										}); 
										$("#relatedJobProfileBlock").html(relatedJobProfiles);
									}
									catch(err)
									{}								
								
									try
									{
										var skillOntology = obj.Skill.SkillData.SkillOntology;
										skillOntology = skillOntology.split(">");
										var ontologyText = "";
										$.each(skillOntology,function(i)
										{
											 ontologyText = ontologyText+'<div class="btn btn-default ontology-btn">';
											 ontologyText = ontologyText+'<b>'+skillOntology[i]+'</b></div>';
											 if(i < skillOntology.length-1)
											 {
												 ontologyText = ontologyText+'<i class="fa fa-arrow-right fa-icon" aria-hidden=""></i>';
											 }
										});
	
										$("#skillOntology").html(ontologyText);
									}
									catch(err)
									{}
									
									$('#skillOutput').html();
									$('#skillOutput').html(respData);
									try
									{
										$('#skillOutput').beautifyJSON({
											type : "strict"
										});
									}catch(err){}
									$("#loader").hide(); 
									$("#skillErrorMessage").hide();
									$("#skillResponse").show();
								}
								else
								{
									$("#skillResponse").hide();
									$("#skillErrorMessage").show();
									$("#loader").hide(); 
								}
							}
							else
							{
								$("#skillResponse").hide();
								$("#apiErrorMessage").show();
								$("#apiErrorMessageText").empty().text(respData);
								$("#loader").hide(); 
							}
						}
						else
						{
							$("#skillResponse").hide();
							$("#skillErrorMessage").show();
							$("#loader").hide(); 
						}
						},
	                    failure: function (response) {
	                    	console.log(response);
	                    	hideLoader();
	                    },
	                    error: function (response) {
	                    	console.log(response);
	                       hideLoader();
						}
	                });
			}
		}
		catch(err)
		{
			console.log(err);
        	hideLoader();
		}
	}

	//get job profile detail
	$("#btnJobProfile").click(function()
	{
		var searchText = $("#txtJobProfile").val();
		getJobProfileDetail(searchText);
	});

	$('#txtJobProfile').keydown(function(event){    
	    if(event.keyCode==13){
	    	var searchText = $("#txtJobProfile").val();
			getJobProfileDetail(searchText);
	    }
	});
	
	$(document).on("click", ".skillRelatedJobProfile", function()
	{
		var searchText = $(this).val();
		getJobProfileDetail(searchText);
		try{
			$("#open-result-jobprofile").addClass("active");
			$("#open-result").removeClass("active");
			$("#defaultOpenJP").addClass("defaultOpenSkill");
			document.getElementById("open-result-jobprofile").click();
			document.getElementById("defaultOpenJP").click();
		}catch (e) {
			console.log('error ::'+e);
		}
	});
	
	function getJobProfileDetail(searchText)
	{
		
		$("#apiErrorMessage").hide();
		$("#jobProfileErrorMessage").hide();
		try
		{
			if(searchText != null && searchText != "")
			{
				showLoader();
				var searchInUrl = URL+"jobprofilesearch";
				var jsonData = { ApiKey : key, Version : version, Language : language, Locale : APILocale, CustomValues : customValues, Keyword : ""+searchText+""};
				$.ajax({
	                    type: "POST",
	                    url: searchInUrl,
	                    data:  JSON.stringify(jsonData),
	                    contentType: "application/json", 
	                    dataType: "json",
	                    success: function (response) {
	                    
		                var respData= JSON.stringify(response, undefined, 4);
		                if(respData != null && respData != "") 
						{
							if(respData.indexOf("errorcode") == -1)
							{
								$('.outputJBDiv').show();
								var obj = JSON.parse(respData);
								if(obj.JobProfile.JobProfileData.FormattedJobProfile != null && obj.JobProfile.JobProfileData.FormattedJobProfile != "")
								{
									$("#searchJobProfile").val(obj.JobProfile.SearchKeyword);
									$("#formattedJobProfile").val(obj.JobProfile.JobProfileData.FormattedJobProfile);
									$("#jobProfileDescription").html(obj.JobProfile.JobProfileData.Description);
									try{
										openTab(event, 'JobRawJsonOutput');
										$('#defaultOpenJP').addClass('active');
									}catch (e) {
									}	
									try
									{
										var skillsC = '';
										$.each(obj.JobProfile.JobProfileData.Alias, function(index, val) { 
											if(index !==0)
											{
												skillsC = skillsC +", ";
											}
											skillsC = skillsC+val;
										});
										$("#jobProfileAlias").html(skillsC);
									}
									catch(err)
									{
										
									}

									try
									{
										var relatedSkills = "";
										$.each(obj.JobProfile.JobProfileData.Skills, function(key,value) 
										{
											
											relatedSkills = relatedSkills+'<div class="col-md-4 ">';
											relatedSkills = relatedSkills+'<label class=" pt-3 lable-form">Skill</label>';
											relatedSkills = relatedSkills+'<div class="form-group">';
											relatedSkills = relatedSkills+'<input type="text" value="'+value.Skill+'" readonly="" class="link-bar-3 w-100 jobProfileSkills" placeholder="">';
											relatedSkills = relatedSkills+'</div></div>';
											relatedSkills = relatedSkills+'<div class="col-md-4 ">';
											relatedSkills = relatedSkills+'<label class=" pt-3 lable-form">Proficiency Level</label>';
											relatedSkills = relatedSkills+'<div class="form-group">';
											relatedSkills = relatedSkills+'<input type="text" value="'+value.ProficiencyLevel+'" class="link-bar-2 w-100" placeholder="">';
											relatedSkills = relatedSkills+'</div></div>';
											relatedSkills = relatedSkills+'<div class="col-md-4 ">';
											relatedSkills = relatedSkills+'<label class=" pt-3 lable-form">Skill Type</label>';
											relatedSkills = relatedSkills+'<div class="form-group">';
											relatedSkills = relatedSkills+'<input type="text" value="'+value.SkillType+'" class="link-bar-2 w-100" placeholder="">';
											relatedSkills = relatedSkills+'</div></div>';
										
										}); 
										$("#jobProfileSkillsBlock").empty().append(relatedSkills);
									}
									catch(err)
									{}
	
									try
									{
										var tasks = "";
										$.each(obj.JobProfile.JobProfileData.Tasks, function(key,value) 
										{
											tasks = tasks+'<p><b><i class="glyphicon glyphicon-check"></i></b>  '+value+'</p>';
										});
										$("#tasksBlock").html(tasks);
									}
									catch(err)
									{
										
									}

									try
									{
										var abilities = "";
										$.each(obj.JobProfile.JobProfileData.Abilities, function(key,value) 
										{
											abilities = abilities+'<p><b><i class="glyphicon glyphicon-check"></i></b>  '+value+'</p>';
										});
										$("#abilitiesBlock").html(abilities);
									}
									catch(err)
									{}

									try
									{
										var knowledge = "";
										$.each(obj.JobProfile.JobProfileData.Knowledge, function(key,value) 
										{
											knowledge = knowledge+'<p><b><i class="glyphicon glyphicon-check"></i></b>  '+value+'</p>';
										});
										$("#knowledgeBlock").html(knowledge);
									}
									catch(err)
									{}									

									try
									{
										var education = "";
										$.each(obj.JobProfile.JobProfileData.Education, function(key,value) 
										{
											education = education+'<p><b><i class="glyphicon glyphicon-check"></i></b>  '+value+'</p>';
											//education = "";
										});
										$("#educationBlock").html(education);
									}
									catch(err)
									{
										console.log('Error Eduction:: '+err);
									}

									try
									{
										var workActivities = "";
										$.each(obj.JobProfile.JobProfileData.WorkActivities, function(key,value) 
										{
											workActivities = workActivities+'<p><b><i class="glyphicon glyphicon-check"></i></b>  '+value+'</p>';
										});
										$("#workActivitiesBlock").html(workActivities);
									}
									catch(err)
									{}

									try
									{
										var jobProfileOntology = obj.JobProfile.JobProfileData.JobProfileOntology;
										jobProfileOntology = jobProfileOntology.split(">");
										var ontologyText = "";
										$.each(jobProfileOntology,function(i)
										{
											ontologyText = ontologyText+'<div class="btn btn-default ontology-btn">';
											ontologyText = ontologyText+'<b>'+jobProfileOntology[i]+'</b></div>';
											if(i < jobProfileOntology.length-1)
											{
												ontologyText = ontologyText+'<i class="fa fa-arrow-right fa-icon" aria-hidden="true"></i>';
											}
										});
										$("#ontologyJobProfileBlock").html(ontologyText);
									}
									catch(err)
									{}
	
									$('#jobProfileOutput').empty();
									$('#jobProfileOutput').html(respData);
									try
									{
										$('#jobProfileOutput').beautifyJSON({
											type : "strict"
										});
									}catch(err){}
									$("#loader").hide(); 
									$("#jobProfileErrorMessage").hide();
									$("#jobProfileResponse").show();
								}
								else
								{
									$("#jobProfileResponse").hide();
									$("#jobProfileErrorMessage").show();
									$("#loader").hide(); 
								}
							}
							else
							{
								$("#jobProfileResponse").hide();
								$("#jobProfileErrorMessage").show();
								$("#jobProfileErrorMessageTxt").empty().text(respData);
								$("#loader").hide(); 
							}
						}
						else
						{
							$("#jobProfileResponse").hide();
							$("#jobProfileErrorMessage").show();
							$("#loader").hide(); 
						}
						},
	                    failure: function (response) {
	                    	console.Log(response);
	                    	hideLoader();
	                    },
	                    error: function (response) {
	                    	console.Log(response);
	                    	hideLoader(); 
						}
	                });
			}
		}
		catch(err)
		{
			console.Log(err);
        	hideLoader();
		}
	}